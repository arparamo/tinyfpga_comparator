# TinyFPGA: pulse comperator, signal reponse to input signal.
**Author**: Angel Rodriguez Paramo (arparamo@essbilbao.org / arparamo@gmail.com)

## Description

This code gives a long pulse output (powering a LED) if a pulse input is higher than a threshold:
 - Signal input: generated with a AWG (picoscope 2024)
 - Threshold: generated with the tiniFPGA through PWM and capacitors.
 - Signal comparison: using a opamp.
 - Pulse generation: in the tinyFPA if a small pulse comes from the opAmp with duration of approx. 1 us, it can be converted to a long pulse ouput.

Somehow the test wants to representa Machine Protection signal, where a small input should generate an interlock.

<figure>
<img src="doc/scheme.jpg" alt="alt text" width="400">
  <figcaption>
  <b>Set-up for tinyFPGA test using also a pico 2024A as signal generator, and leds for signals output.</b>
</figcaption>
</figure>

## Installation

From `https://tinyfpga.com/bx/guide.html`

Install tinyprog:
```
pip install apio==0.4.0 tinyprog
apio install system scons icestorm iverilog
apio drivers --serial-enable
```
Check bootloader
```
tinyprog --update-bootloader
```

Install Atom IDEs and apio packages.

## Execution

Upload with Atom using the APIO toolbar.

The code is:
-  `top.v` verilog code
- `pins.pcf` fpga pinning

For doing it manually (install also yosys, arachne-pnr and icepack):
```
yosys -p "synth_ice40 -blif hardware.blif" -q top.v pll.v
arachne-pnr -d 8k -P cm81 -p pins.pcf -o hardware.asc -q hardware.blif
icepack hardware.asc hardware.bin
tinyprog -c /dev/ttyS3 -p hardware.bin # In linux # or COM3 in windows
```
For viewing the FPGA floormap `https://github.com/knielsen/ice40_viewer`


// Code for pulse compartor.
// Author**: Angel Rodriguez Paramo (arparamo@essbilbao.org / arparamo@gmail.com)
//
// Code based onT inyFPGA templates https://github.com/tinyfpga/TinyFPGA-BX

// look in pins.pcf for all the pin names on the TinyFPGA BX board
module top (
    input CLK,    // 16MHz clock

    input PIN_1,    // Input Pin for led signal

    output LED,   // User/boot LED next to power LED
    output USBPU,  // USB pull-up resistor
    output PIN_20,

    output PIN_2,   
);
    // drive USB pull-up resistor to '0' to disable USB
    assign USBPU = 0;

    ////////
    // MAKE AN ANALOGUE OUPUT  (CLK=16 MHZ)
    ////////

    reg [16:0] ao_counter; // keep track of time and location in blink_pattern
    wire [7:0] ao_pattern = 8'b00000011;  // pattern that will be flashed over the LED over time

    // Output voltage is bits in pattern/8 * 3.3 V

    always @(posedge CLK) begin // increment the blink_counter every clock
        ao_counter <= ao_counter + 1; //<= for Non-blocking
    end

    // light up the LED according to the pattern
    assign PIN_20 = ao_pattern[ ao_counter[16:14] ]; // 14 for ~1 ms

    ////////
    // MAKE A PULSE GENERATED ONLY WHEN INPUT IS HIGH
    // Generate a 1 s output if input signal is larger than required value
    ////////
    
    // Define PULLUP resitor. Also possible to do in the pcf file.
    wire in_signal;
    SB_IO #(
      .PIN_TYPE(6'b 0000_01),
      .PULLUP(1'b 0) // 1'b 1 for pullup
    ) u_in_signal(
      .PACKAGE_PIN(PIN_1),
      .D_IN_0(in_signal)
    );

    wire out_signal;
    reg [14:0] input_counter; //14 for 1 ms
    reg [24:0] output_counter; 

   always @(posedge CLK) begin

    	   if (out_signal)
               output_counter = output_counter + 1; 
	   else if (in_signal)
                   input_counter = input_counter + 1; 
	        else // input signal should be continous
                   input_counter = 0; 
           //if
	   
	   if (input_counter[5]) begin //14: 1ms; 7: 8 us; 5: 2 us
    	        out_signal = 1;
    	        input_counter = 0;
	   end //if
		

	   if (output_counter[24]) begin
    	        out_signal = 0;
    	        output_counter = 0;
	   end //if
    end//always

    assign PIN_2 = out_signal ;
    assign LED = in_signal;

endmodule
